package task1_var2;

public class ThreadApp {
    public static void main(String[] args) {

        StringBuffer text = new StringBuffer();

        CountClass c1 = new CountClass(text, 200);
        CountClass c2 = new CountClass(text, 100);
        Thread t = new Thread(c1);

        t.start();

        c2.run();
        System.out.println("Result: " + text);

    }
}
