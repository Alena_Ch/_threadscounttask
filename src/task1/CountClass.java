package task1;

public class CountClass extends Thread {
    private StringBuffer text;
    private int countTo;

    public CountClass(StringBuffer s, int c) {
        text = s;
        countTo = c;
    }

    @Override
    public void run() {
        try {
            join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int s = 0;
        int sum = 0;
        for (int i = 0; i < countTo+1; i++) {
            s = s + i;
            sum = +s;
        }
        text.append("\nsum=" + sum + "\n");
    }
}
