package task1;

public class ThreadsApp {
    public static void main(String[] args) {

        StringBuffer text = new StringBuffer();
        CountClass c1 = new CountClass(text, 200);
        Thread t = new Thread(c1);
        t.start();

        CountClass c2 = new CountClass(text, 100);
        c2.run();
        System.out.println("Result: " + text);

    }
}
